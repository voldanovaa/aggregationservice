package com.sem.rest.aggregationservice;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class AggregService {


    public int getTicketId(int id){
        return id;
    }

    public String getTicketsServiceNow(int id){
        URL url = null;
        StringBuffer content = null;

        try{
            //url = new URL("http://localhost:8083/api/v1/tickets/1");
            url = new URL("http://localhost:8083/api/v1/tickets/" + getTicketId(id));

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            content = new StringBuffer();
            while ((inputLine = in.readLine()) != null){
                content.append(inputLine);
            }
            in.close();
            connection.disconnect();

        }catch(ProtocolException e){
            e.printStackTrace();
        }catch (MalformedURLException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }

        return content.toString();
    }
}

