package com.sem.rest.aggregationservice;

import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;


@RestController
@RequestMapping("/aggreg")
public class AggregController {

    AggregService aggregService = new AggregService();

    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/test")
    public String testing(@RequestParam(value = "name", defaultValue = "World") String name) {
        return "AGGREGATION MODULE" + name + counter;
    }

    @GetMapping("/serviceNow")
    public String getTicketById(@RequestParam(value = "id",defaultValue = "Ticket") String name){
        return aggregService.getTicketsServiceNow(2);
    }

}











